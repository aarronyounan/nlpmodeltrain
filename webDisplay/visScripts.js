

        $.ajax({
            datatype: "json",
          url: 'dataset.json',
          success: function (data) {

        var startDate = (moment().subtract(30,'minutes')).toISOString();
        var endDate = (moment().add(30,'minutes')).toISOString();
            // hide the "loading..." message
            //document.getElementById('loading').style.display = 'none';

            // DOM element where the Timeline will be attached
            var container = document.getElementById('visualization');

            // Create a DataSet (allows two way data-binding)
            var items = new vis.DataSet(data);

            // Configuration for the Timeline
            var options = {
                height:"400px",
                start: startDate,
                end: endDate
            };

            // Create a Timeline
            var timeline = new vis.Timeline(container, items, options);
            var interval = setInterval(refreshTL,1000);
            timeline.on("mouseDown",function(properties){
                displayQuoteMouse(properties,items);
            });
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            timeline.on("select",function(itemSelected){
                displayQuoteSelect(itemSelected,items);
            });
}


            document.getElementById('zoomIn').onclick=function(){timeline.zoomIn(0.2);};
            document.getElementById('zoomOut').onclick=function(){timeline.zoomOut(0.2);};
            document.getElementById('rewindDay').onclick=function(){rewind(60*24,items);};
            document.getElementById('rewindHour').onclick=function(){rewind(60,items);};
            document.getElementById('ffHour').onclick=function(){ff(60);};
            document.getElementById('ffDay').onclick=function(){ff(60*24);};
            document.getElementById('nowButton').onclick=function(){resetView();};

            function refreshTL(){
                earliestTime = items.get(0)["start"];
                
            $.ajax({url:'dataset.json',
            success: function(data){
                items.update(data);
                var numBooks = 3032;
                var numItems = items.getIds().length;
                var percentage = (numItems/numBooks)*100;
                latestTime=items.get(items.length-1)["start"];
                avg = (numItems)/(moment(latestTime).diff(moment(earliestTime),'hours',true));
                daysLeft = (numBooks/avg)/24;

                updateAvg(avg);
                updateDaysLeft(daysLeft);
                updateProgress(percentage, numItems,numBooks);
            }});}

            function stringifyObject (object) {
            if (!object) return;
                var replacer = function(key, value) {
                  if (value && value.tagName) {
                    return "DOM Element";
                  } else {
                    return value;
                  }
                };
        return JSON.stringify(object, replacer);
        }
        function updateAvg(newAvg){
            var avgSpan = document.getElementById('bookNumber');
            avgSpan.innerHTML = newAvg.toFixed(1) +" books/hour";
        }
        function updateDaysLeft(days){
            var daysSpan = document.getElementById('daysLeft');
            daysSpan.innerHTML = days.toFixed(1);
        }
        function updateProgress(percentage,numItems,numBooks){
            var progBar = document.getElementById('progBar');
            var progCount = document.getElementById('progCount');
            progBar.style.visibility="visible";
            progBar.style.width=(percentage.toFixed(1)+"%");
            progBar.innerHTML = percentage.toFixed(1)+"%";
            progCount.innerHTML=(numItems+"/"+numBooks);
        }

        function displayQuoteMouse(properties, items){
            var quoteDiv = document.getElementById('quote');
            var quoteHeader = document.getElementById('quoteHeader');
            if(properties.item["quote"]!=='undefined'){
               quoteDiv.innerHTML = items.get(properties.item)["quote"];
            }
        }

        function displayQuoteSelect(selectedItem,dataSet){
            var quoteDiv = document.getElementById('quote');
            var quoteHeader = document.getElementById('quoteHeader');
            var itemNum = selectedItem["items"][0];
            if(dataSet.get(itemNum)["quote"]!==undefined){
               quoteDiv.innerHTML = dataSet.get(itemNum)["quote"];
            }
        }

            function resetView(){
                timeline.setWindow({
                    start: (moment().subtract(30,'minutes')).toISOString(),
                    end: (moment().add(30,'minutes')).toISOString()
                });
            }

            function rewind (minutes,items) {
                var range = timeline.getWindow();
                var interval = range.end - range.start;
                earliestPossible = (moment(items.get(0)["start"]).subtract(15,'minutes')).toISOString();
                startTime =(moment(range.start).subtract(minutes,'minutes')).toISOString();
                endTime = (moment(range.end).subtract(minutes,'minutes')).toISOString();
                console.log(earliestPossible);
                if(moment(startTime).isAfter(earliestPossible)){
                    timeline.setWindow({
                        start: startTime,
                        end: endTime
                    });
                }else{
                    timeline.setWindow({
                        start: earliestPossible,
                        end: (moment(earliestPossible).add(interval,'milliseconds')).toISOString()
                    });
                }
            }
            function ff (minutes) {
                var range = timeline.getWindow();
                var interval = range.end - range.start;
                console.log("Moving by ");
                console.log(interval);
                latestPossible = moment().add(15,'minutes').toISOString();
                startTime =(moment(range.start).add(minutes,'minutes')).toISOString();
                endTime = (moment(range.end).add(minutes,'minutes')).toISOString();
                if(moment(endTime).isBefore(latestPossible)){
                    timeline.setWindow({
                        start: startTime,
                        end: endTime
                    });
                }else{
                    timeline.setWindow({
                        start: (moment(latestPossible).subtract(interval,'milliseconds')).toISOString(),
                        end: latestPossible
                    });
                }
            }

          },
          error: function (err) {
            console.log('Error', err);
            if (err.status === 0) {
              alert('Failed to load data/basic.json.\nPlease run this example on a server.');
            }
            else {
              console.log(err.status);
              alert('Failed to load data/basic.json.');
            }
          }
        });

