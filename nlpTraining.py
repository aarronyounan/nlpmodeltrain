from __future__ import absolute_import, print_function, division
import os
from six import moves
import ssl
import pyttsx
import time
import gtts as gt
import re
import tflearn
from tflearn.data_utils import *
from os import listdir
from os.path import isfile, join
import json
from tensorflow.contrib.tensorboard.plugins import projector


def generateCharIdx(onlyfiles, folder):
    print("Generating Char ID")
    chars = set()
    for fileNumber, fileName in enumerate(onlyfiles):
        path = os.path.join(folder, fileName)
        print(fileName)
        try:
            trainingString = open(path, 'r').read()
            chars = chars.union(set(trainingString))
            print(str(fileNumber) + "/" + str(len(onlyfiles)))
        except Exception as e:
            print(e)
            pass
    char_idx = {c: i for i, c in enumerate(sorted(chars))}
    # print(char_idx)
    return char_idx


def updateAlreadyReadBooks(bookTitle):
    txtFile = open('Gutenberg/alreadyRead.txt', 'a')
    txtFile.write(bookTitle+'\n')
    txtFile.close()
    return


def jsonStatWriter(index,training_state, bookTitle, quote):
    jsonFile = open('webDisplay/dataset.json', 'r+')
    jsonObj = json.load(jsonFile)
    book = formatTitle(bookTitle)
    dict = {
            'id':index,
            'content': book[0],
            'author':book[1],
            'title':book[2],
            "start": time.strftime("%m/%d/%Y %H:%M:%S", time.localtime()),
            "loss": '{:.4f}'.format(training_state.loss_value),
            "quote": quote
            }
    jsonObj.append(dict)
    jsonFile.seek(0)
    jsonFile.write(json.dumps(jsonObj, sort_keys=True, indent=4, separators=(',', ': '))+"\n")
    jsonFile.flush()
    jsonFile.close()
    return


def formatTitle(rawBookString):
    rawBook = rawBookString.split('___')
    author = rawBook[0]
    title = rawBook[1].replace(".txt", "")
    book = author + " " + title
    return (book,author,title)


def removeAlreadyTrained(masterList):
    with open('Gutenberg/alreadyRead.txt', 'r+') as csvFile:
        alreadyTrained = [book.strip('\n') for book in csvFile]
        remaining = set(masterList).difference(set(alreadyTrained))
    return remaining

def main():
    model_name = 'baseNLPModel'
    folder = 'Gutenberg/txt/'
    onlyfiles = [f for f in listdir(folder) if isfile(join(folder, f))]
    char_id = generateCharIdx(onlyfiles=onlyfiles, folder=folder)
    maxlen = 30
    remainingBooks = removeAlreadyTrained(onlyfiles)
    print(len(remainingBooks))
    graph = tflearn.input_data([None, maxlen, len(char_id)])
    graph = tflearn.lstm(graph, 512, return_seq=True, forget_bias=5.0)  # char WRT char
    graph = tflearn.dropout(graph, 0.5)
    graph = tflearn.lstm(graph, 512, name='LSTM1', return_seq=True, forget_bias=5.0)  # word WRT word
    graph = tflearn.dropout(graph, 0.5, name='DO1')
    graph = tflearn.lstm(graph, 512, name='LSTM2', return_seq=False, forget_bias=5.0)  # sentence WRT sentence
    graph = tflearn.dropout(graph, 0.6, name='DO2')
    # graph = tflearn.lstm(graph,512,name='LSTM3',forget_bias=4.0) #ideas WRT to ideas
    # #graph = tflearn.layers.embedding_ops.embedding(graph,512,len(char_id))
    # graph = tflearn.dropout(graph,0.3,name='DO3')
    graph = tflearn.fully_connected(graph, len(char_id), activation='softmax')
    graph = tflearn.regression(graph, optimizer='adam', loss='categorical_crossentropy',
                               learning_rate=0.001)
    model = tflearn.SequenceGenerator(graph, char_id, maxlen, 5.0, 3, 'logs/', checkpoint_path=
    './checkpointPath/' + model_name)
    #model.load('./checkpointPath/' + model_name + '-467602')
    sequenceLength = 600
    temperature = 0.9
    # seed = random_sequence_from_string(trainingString, maxlen)
    # print(model.generate(sequenceLength, 0.07, seed))
    jsonIndex=0
    for fileName in remainingBooks:
        path = os.path.join(folder, fileName)
        try:
            trainingString = open(path, 'r').read()
            X, Y, char_idX = string_to_semi_redundant_sequences(trainingString, maxlen, 3, char_id)
            print(fileName)
            seed = random_sequence_from_string(trainingString, int(maxlen))
            training_state = model.trainer.training_state
            model.fit(X, Y, validation_set=0.1, batch_size=250, n_epoch=1,
                      run_id=(model_name + time.strftime('_%m%D%Y_%H%M%S')))
            quote = model.generate(sequenceLength, temperature, seed).encode('utf-8')
            jsonStatWriter(jsonIndex, training_state, fileName, quote)
            jsonIndex+=1
            updateAlreadyReadBooks(fileName)
            temp1Out = "Temp " + str(temperature) + ": " + quote
            print('\n\n' + temp1Out + '\n\n')
        except Exception as e:
            print("Something went wrong")
            print(e)

        except KeyboardInterrupt as kInt:
            print(kInt)
            break

    model.save("./checkpointPath/" + model_name + ".model")
    return


if (__name__ == '__main__'):
    main()
