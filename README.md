This is the code to (pre)train an RNN for NLP Sequence Generation on the 
Gutenberg dataset (or any other multi-file user-supplied corpus) using TF-Learn and 
display results on a vis.js timeline.

